/* 
 * This file is part of rrm.
 * 
 * Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef _RRM_H_
# define _RRM_H_

typedef struct rrm_s rrm_t;
typedef struct rrm_node_s rrm_node_t;
typedef struct rrm_receipt_s rrm_receipt_t;
typedef struct rrm_repayment_s rrm_repayment_t;

struct rrm_list_s {
    rrm_node_t * first;
    rrm_node_t * last;
};
typedef struct rrm_list_s rrm_list_t;

enum rrm_error_e {
    RRM_ERROR_NONE = 0,
    RRM_ERROR_ERRNO, /* Error is defined by errno. */
    RRM_ERROR_REMOVED,
    RRM_ERROR_REPAYED,
    RRM_ERROR_TIME
};
typedef enum rrm_error_e rrm_error_t;


/** RRM handling. **/

const char * rrm_strerror(const rrm_error_t error);

rrm_t * rrm_create(const char * const filename)
    __attribute__((nonnull));

rrm_t * rrm_open(const char * const filename)
    __attribute__((nonnull));

void rrm_close(rrm_t * const rrm);

int rrm_save(rrm_t * const rrm) __attribute__((nonnull));

int rrm_is_saved(const rrm_t * const rrm)
    __attribute__((nonnull));

void rrm_print_total(const rrm_t * const rrm, const float total)
    __attribute__((nonnull));

/* This function determines if two amounts are equal, that is they
 * differ by less than the significance.
 * Returns 1 if the amounts are equal and 0 otherwise. */
int rrm_amount_is_eq (const float amount1, const float amount2)
    __attribute__((const));


/** Receipt handling **/

rrm_receipt_t * rrm_receipt_new(rrm_t * rrm,
                                const char * const name,
                                const float amount)
    __attribute__((nonnull));

/* Returns 1 if the receipt is removed and 0 otherwise. */
int rrm_receipt_is_removed(const rrm_receipt_t * const receipt)
    __attribute__((nonnull, pure));

/* Returns 1 if the receipt has been repayed and 0 otherwise. */
int rrm_receipt_is_repayed(const rrm_receipt_t * const receipt)
    __attribute__((nonnull, pure));

rrm_receipt_t * rrm_receipt_get_from_id(const rrm_t * const rrm,
                                        const unsigned int id)
    __attribute__((nonnull));

int rrm_receipt_remove_from_id(rrm_t * const rrm, const unsigned int id)
    __attribute__((nonnull));

void rrm_receipt_header_print (const rrm_t * const rrm)
    __attribute__((nonnull));
void rrm_receipt_print(const rrm_t * const rrm,
                       const rrm_receipt_t * const receipt)
    __attribute__((nonnull));

void rrm_receipt_print_active(const rrm_t * const rrm)
    __attribute__((nonnull));

void rrm_receipt_print_all(const rrm_t * const rrm)
    __attribute__((nonnull));


/** Repayment handling **/

rrm_repayment_t * rrm_repayment_create(rrm_t * const rrm)
    __attribute__((nonnull));

rrm_error_t
rrm_repayment_add_receipt(rrm_t * const rrm,
                          rrm_repayment_t * const repayment,
                          rrm_receipt_t * const receipt)
    __attribute__((nonnull));
rrm_repayment_t * rrm_repayment_get_from_id(const rrm_t * const rrm,
                                            const unsigned int id)
    __attribute__((nonnull));

/* This function aquires the date from the repayment and sets the data
 * pointed to by the pointers correspondingly.
 * Returns 0 on success and 1 on failure. */
int rrm_repayment_date(const rrm_repayment_t * const repayment,
                       int * const year,
                       int * const month,
                       int * const day)
    __attribute__((nonnull));

void rrm_repayment_header_print (const rrm_t * const rrm)
    __attribute__((nonnull));
void rrm_repayment_print(const rrm_t * const rrm,
                         const rrm_repayment_t * const repayment)
    __attribute__((nonnull));
void rrm_repayment_print_all (const rrm_t * const rrm)
    __attribute__((nonnull));


/** Node handling **/

rrm_node_t * rrm_node_next (const rrm_node_t * const node)
    __attribute__((nonnull));
rrm_receipt_t * rrm_node_receipt (const rrm_node_t * const node)
    __attribute__((nonnull));


/** List handling. **/

void rrm_list_init (rrm_list_t * const list) __attribute__((nonnull));
void rrm_list_destroy (rrm_list_t * const list)
    __attribute__((nonnull));
rrm_node_t * rrm_list_first (const rrm_list_t * const list)
    __attribute__((nonnull));
void rrm_list_print(const rrm_t * const rrm, const rrm_list_t * const list)
    __attribute__((nonnull));

/* This function will find the combination of unrepayed and unremoved
 * receipts that form the smallest total amount equal to or greater
 * than the amount argument.
 * 
 * The list should have been initialised with rrm_list_init() before
 * this call. The list will be filled in with the optimum combination
 * of receipts.
 * 
 * If no combination with an amount greater than or equal to the wanted
 * amount all receipts are added to list and *best_amount is less than
 * amount.
 * 
 * Returns 0 on success or 1 on error. */
int rrm_fit_repayment(const rrm_t * const rrm,
                      rrm_list_t * const list,
                      float * const best_amount,
                      const float amount)
    __attribute__((nonnull));

#endif /* !_RRM_H */
