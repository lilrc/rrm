/* 
 * This file is part of rrm.
 * 
 * Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

/* errno */
#include <errno.h>

/* va_end(), va_list, va_start(), vfprintf() */
#include <stdarg.h>

/* false */
#if HAVE_STDBOOL_H
# include <stdbool.h>
#endif /* HAVE_STDLIB_H */

/* EXIT_SUCCESS */
#include <stdlib.h>

/* fprintf(), sprintf(), stderr */
#include <stdio.h>

/* strcpy(), strdup() */
#include <string.h>

/* access() */
#if HAVE_UNISTD_H
# include <unistd.h>
#endif /* HAVE_UNISTD_H */

#include <gop.h>
#include <rrm.h>

#include "cli.h"

/* strlen("Remainder receipt of repayment XXXX-XX-XX") */
#define STRLEN_REMAINDER_RECEIPT 41

static void __attribute__((nonnull))
print_error(const char * const fmt, ...)
{
    fputs(PACKAGE ": ", stderr);

    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);

    fputc('\n', stderr);

    return;
}

static void
print_version(void)
{
    const char * const license = "\
License GPLv3+: GNU GPL version 3 or later\n\
<http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n";

    printf("%s\n"
           "Copyright (C) 2013 Karl Linden\n"
           "%s", PACKAGE_STRING, license);
    return;
}

static void __attribute__((nonnull))
print_version_cli(cli_t * const cli)
{
    print_version();
    return;
}

static gop_return_t __attribute__((nonnull))
print_version_gop(gop_t * const gop)
{
    print_version();
    return GOP_DO_EXIT;
}

static gop_return_t __attribute__((nonnull))
aterror_callback(gop_t * const gop)
{
    const gop_error_t error = gop_get_error(gop);
    switch (error) {
        case GOP_ERROR_EXPARG:
        case GOP_ERROR_UNKLOPT:
        case GOP_ERROR_UNKSOPT:
            gop_default_error_handler(gop);
            putchar('\n');
            gop_print_help(gop);
            gop_set_exit_status(gop, EXIT_FAILURE);
            return GOP_DO_EXIT;
        default:
            return GOP_DO_CONTINUE;
    }
}

static void __attribute__((nonnull))
add_receipt(cli_t * const cli)
{
    rrm_t * const rrm = cli_get_pointer(cli);
    char * const name = cli_input_string("Receipt name");
    float amount;
    if (cli_input_float("Receipt amount", &amount)) {
        free(name);
        return;
    }

    if (amount <= 0) {
        cli_print_error("Invalid ammount");
        free(name);
        return;
    }

    rrm_receipt_t * const receipt = rrm_receipt_new(rrm, name, amount);
    if (receipt == NULL) {
        free(name);
        return;
    }

    puts("This receipt was added:");
    rrm_receipt_header_print(rrm);
    rrm_receipt_print(rrm, receipt);

    free(name);
    return;
}

static void __attribute__((nonnull))
print_receipt(cli_t * const cli)
{
    const rrm_t * const rrm = cli_get_pointer(cli);
    unsigned int id;
    if (cli_input_uint("Receipt ID", &id)) {
        return;
    }
    const rrm_receipt_t * const receipt =
        rrm_receipt_get_from_id(rrm, id);
    if (receipt == NULL) {
        cli_print_error("Invalid receipt ID");
        return;
    }
    rrm_receipt_header_print(rrm);
    rrm_receipt_print(rrm, receipt);
    return;
}

static void __attribute__((nonnull))
print_active_receipts(cli_t * const cli)
{
    const rrm_t * const rrm = cli_get_pointer(cli);
    rrm_receipt_print_active(rrm);
    return;
}


static void __attribute__((nonnull))
print_all_receipts(cli_t * const cli)
{
    const rrm_t * const rrm = cli_get_pointer(cli);
    rrm_receipt_print_all(rrm);
    return;
}

static void __attribute__((nonnull))
remove_receipt(cli_t * const cli)
{
    /* Ask the user what receipt is to be removed. */
    rrm_t * const rrm = cli_get_pointer(cli);
    unsigned int id;
    if (cli_input_uint("Receipt ID", &id)) {
        return;
    }

    /* Get the receipt so that it can be printed. */
    rrm_receipt_t * const receipt = rrm_receipt_get_from_id(rrm, id);
    if (receipt == NULL) {
        cli_print_error("Invalid receipt ID");
        return;
    }

    /* Print the receipt. */
    rrm_receipt_header_print(rrm);
    rrm_receipt_print(rrm, receipt);

    /* Notify the user if the receipt has already been removed. */
    if (rrm_receipt_is_removed(receipt)) {
        cli_print_error("The receipt has already been removed.");
        return;
    }

    /* Notify the user if the receipt has been repayed. It is impossible
     * to remove repayed receipts. */
    if (rrm_receipt_is_repayed(receipt)) {
        cli_print_error("The receipt is repayed. Repayed receipts "
                        "cannot be removed.");
        return;
    }

    /* Ask for confirmation. */
    const int ret = cli_input_yesno("Are you sure you want to remove "
                                    "the receipt?");
    if (ret < 0) {
        cli_perror(NULL);
        return;
    } else if (ret == 0) {
        puts("Not confirmed");
        return;
    }

    /* Remove the receipt. */
    if (rrm_receipt_remove_from_id(rrm, id)) {
        cli_perror("Could not remove receipt");
        return;
    }

    /* Inform the user the receipt was removed. */
    puts("This receipt has been removed:");
    rrm_receipt_header_print(rrm);
    rrm_receipt_print(rrm, receipt);

    return;
}

static void __attribute__((nonnull))
print_repayment(cli_t * const cli)
{
    const rrm_t * const rrm = cli_get_pointer(cli);
    unsigned int repayment_id;
    if (cli_input_uint("Repayment ID", &repayment_id)) {
        return;
    }
    const rrm_repayment_t * const repayment =
        rrm_repayment_get_from_id(rrm, repayment_id);
    if (repayment == NULL) {
        cli_print_error("Invalid repayment ID");
        return;
    }

    rrm_repayment_header_print(rrm);
    rrm_repayment_print(rrm, repayment);
    return;
}

static void __attribute__((nonnull))
print_all_repayments(cli_t * const cli)
{
    const rrm_t * const rrm = cli_get_pointer(cli);
    rrm_repayment_print_all(rrm);
}

static void __attribute__((nonnull))
create_new_repayment(cli_t * const cli)
{
    rrm_t * const rrm = cli_get_pointer(cli);
    rrm_repayment_t * const repayment = rrm_repayment_create(rrm);
    if (repayment == NULL) {
        cli_perror("Could not create repayment");
        return;
    }

    puts("This repayment was added:");
    rrm_repayment_header_print(rrm);
    rrm_repayment_print(rrm, repayment);
    return;
}

static void __attribute__((nonnull))
add_receipt_to_repayment(cli_t * const cli)
{
    rrm_t * const rrm = cli_get_pointer(cli);
    unsigned int repayment_id;
    if (cli_input_uint("Repayment ID", &repayment_id)) {
        return;
    }
    rrm_repayment_t * const repayment =
        rrm_repayment_get_from_id(rrm, repayment_id);
    if (repayment == NULL) {
        cli_print_error("Invalid repayment ID");
        return;
    }

    unsigned int receipt_id;
    if (cli_input_uint("Receipt ID", &receipt_id)) {
        return;
    }
    rrm_receipt_t * const receipt =
        rrm_receipt_get_from_id(rrm, receipt_id);
    if (receipt == NULL) {
        cli_print_error("Invalid receipt ID");
        return;
    }

    const rrm_error_t error = rrm_repayment_add_receipt(rrm, repayment,
                                                        receipt);
    if (error) {
        if (error == RRM_ERROR_ERRNO) {
            cli_perror("Could not add receipt to repayment");
        } else {
            cli_print_error("Could not add receipt to repayment: %s",
                            rrm_strerror(error));
        }
        return;
    }

    rrm_repayment_header_print(rrm);
    rrm_repayment_print(rrm, repayment);
    puts("The following receipt was added to the repayment above:");
    rrm_receipt_header_print(rrm);
    rrm_receipt_print(rrm, receipt);

    return;
}

static void __attribute__((nonnull))
repay_amount(cli_t * const cli)
{
    rrm_t * const rrm = cli_get_pointer(cli);

    float amount;
    if (cli_input_float("Repayment amount", &amount)) {
        return;
    }

    float best_amount;
    rrm_list_t list;
    rrm_list_init(&list);
    if (rrm_fit_repayment(rrm, &list, &best_amount, amount)) {
        cli_perror("Could not fit the repayment");
        goto end;
    }

    /* Print the list, the total and the differnece. */
    printf("Here follows the best combination of receipts that match\n"
           "the requested repayment amount.\n"
           "Difference from the requested amount is %.2f.\n",
           best_amount - amount);
    rrm_list_print(rrm, &list);
    rrm_print_total(rrm, best_amount);

    const int ret1 = cli_input_yesno("Do you want to repay the above "
                                     "receipts?");
    if (ret1 == -1 || ret1 == 0) {
        goto end;
    }

    /* Create the repayment. */
    rrm_repayment_t * const repayment = rrm_repayment_create(rrm);
    if (repayment == NULL) {
        cli_perror("Could not create repayment");
        goto end;
    }

    /* Iterate over the list and repay the receipts. */
    for (const rrm_node_t * node = rrm_list_first(&list);
          node != NULL;
          node = rrm_node_next(node))
    {
        rrm_receipt_t * const receipt = rrm_node_receipt(node);
        const rrm_error_t error = rrm_repayment_add_receipt(rrm,
                                                            repayment,
                                                            receipt);
        if (error) {
            if (error == RRM_ERROR_ERRNO) {
                cli_perror("Could not add receipt to repayment");
                goto end;
            } else {
                cli_print_error("Could not add receipt to repayment: "
                                "%s", rrm_strerror(error));
                goto end;
            }
        }
    }

    /* Notify the user about the created repayment. */
    puts("This repayment was created:");
    rrm_repayment_header_print(rrm);
    rrm_repayment_print(rrm, repayment);

    if (rrm_amount_is_eq(best_amount, amount)) {
        goto end;
    } else if (best_amount < amount) {
        puts("Requested amount exceeded the available amount. No "
             "remainder receipt can be created.");
        goto end;
    }
    const int ret2 = cli_input_yesno("Do you want a create a receipt "
                                     "from the remainder?");
    if (ret2 == -1 || ret2 == 0) {
        goto end;
    }

    /* Create a remainder receipt. */
    char name[STRLEN_REMAINDER_RECEIPT+1];
    int year, month, day;
    if (rrm_repayment_date(repayment, &year, &month, &day)) {
        strcpy(name, "Remainder of repayment XXXX-XX-XX");
    } else {
        sprintf(name, "Remainder of repayment %4d-%02d-%02d", year,
                month, day);
    }
    const rrm_receipt_t * const receipt = rrm_receipt_new(rrm, name,
                                                          best_amount -
                                                           amount);
    if (receipt == NULL) {
        cli_perror("Could not create receipt");
        goto end;
    }

    /* Notify the user the remainder receipt has been created. */
    puts("This receipt has been created:");
    rrm_receipt_header_print(rrm);
    rrm_receipt_print(rrm, receipt);

end:
    rrm_list_destroy(&list);
    return;
}

static void __attribute__((nonnull))
save_changes(cli_t * const cli)
{
    rrm_t * const rrm = cli_get_pointer(cli);
    if (!rrm_is_saved(rrm)) {
        if (rrm_save(rrm)) {
            cli_perror("Could not save changes");
        } else {
            puts("Changes saved");
        }
    } else {
        puts("No changes to save");
    }
    return;
}

int __attribute__((nonnull))
main(int argc, char ** const argv)
{
    /** Step 1: Parse options. **/
    const gop_option_t options[] = {
        {"version", 'v', GOP_NONE, NULL, &print_version_gop,
            "Print version information and exit", NULL},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    gop_aterror(gop, &aterror_callback);
    gop_add_usage(gop, "[OPTION...] [FILENAME]");
    gop_add_table(gop, NULL, options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    const char * filename = argv[1];
    if (argc > 2) {
        print_error("too many arguments");
        return EXIT_FAILURE;
    }

    /* If there was no filename option on the command line ask for it
     * on the CLI instead. */
    char * filename_cli = NULL;
    if (filename == NULL) {
        filename_cli = cli_input_string("Input the name of the receipt "
                                        "file to use");
        if (filename_cli == NULL) {
            return EXIT_FAILURE;
        }
        filename = filename_cli;
        putchar('\n');
    }


    /** Step 2: Open the rrm file. **/
    rrm_t * rrm = NULL;
    if (access(filename, F_OK)) {
        if (errno == ENOENT) {
            rrm = rrm_create(filename);
        }
    } else {
        rrm = rrm_open(filename);
    }

    /* This is not needed anymore. */
    free(filename_cli);

    if (rrm == NULL) {
        cli_perror("Could not open receipt file");
        return EXIT_FAILURE;
    }

    /** Step 3: Set up and run the CLI. **/
    cli_t cli;
    if (cli_init(&cli, "MAIN MENU")) {
        rrm_close(rrm);
        return EXIT_FAILURE;
    }
    cli_set_pointer(&cli, rrm);

    /* From now on the error goto can be evaluated and in it this
     * variable is needed. */
    int exit_status = EXIT_SUCCESS;

    cli_menu_t * const root = cli_get_root(&cli);
    cli_option_t * option;

#define MENU_OPTION(menu, name, func) { \
    option = cli_option_new(name, func); \
    if (option == NULL) { \
        goto error; \
    } \
    if (cli_menu_add_option(menu, option)) { \
        goto error; \
    } \
}
#define ROOT_OPTION(name, func) MENU_OPTION(root, name, func)

    ROOT_OPTION("Add receipt", add_receipt);
    ROOT_OPTION("Remove receipt", remove_receipt);
    ROOT_OPTION("Print receipt", print_receipt);
    ROOT_OPTION("Print active receipts", print_active_receipts);
    ROOT_OPTION("Print all receipts", print_all_receipts);
    ROOT_OPTION("Repay receipts by amount", repay_amount);

    cli_option_t * const print_repayment_option =
        cli_option_new("Print repayment", print_repayment);
    if (print_repayment_option == NULL) {
        goto error;
    }
    if (cli_menu_add_option(root, print_repayment_option)) {
        goto error;
    }

    cli_option_t * const print_all_repayments_option =
        cli_option_new("Print all repayments", print_all_repayments);
    if (print_all_repayments_option == NULL) {
        goto error;
    }
    if (cli_menu_add_option(root, print_all_repayments_option)) {
        goto error;
    }

    cli_menu_t * const menu =
        cli_menu_new("Manage repayments manually");
    if (menu == NULL) {
        goto error;
    }
    if (cli_menu_add_submenu(root, menu)) {
        goto error;
    }

    MENU_OPTION(menu, "Create a new repayment", create_new_repayment);
    MENU_OPTION(menu, "Add receipt to repayment",
                add_receipt_to_repayment);
    if (cli_menu_add_option(menu, print_repayment_option)) {
        goto error;
    }
    if (cli_menu_add_option(menu, print_all_repayments_option)) {
        goto error;
    }

    ROOT_OPTION("Save changes", save_changes);
    ROOT_OPTION("Print version information", print_version_cli);

    cli_run(&cli);

    if (!rrm_is_saved(rrm)) {
        putchar('\n');
        cli_print_question("Changes have been made since the last "
                           "save.");
        int ret = cli_input_yesno("Do you want to save the changes?");
        if (ret == 1) {
            if (rrm_save(rrm)) {
                cli_perror("Error saving file");
                goto error;
            }
            puts("Changes saved");
        } else if (ret == -1) {
            goto error;
        } else {
            puts("Changes not saved");
        }
    }

    if (false) {
    error:
        exit_status = EXIT_FAILURE;
    }

    rrm_close(rrm);
    cli_destroy(&cli);
    return exit_status;
}
