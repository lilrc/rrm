/* 
 * This file is part of rrm.
 * 
 * Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef CLI_H
# define CLI_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

/* bool */
# if HAVE_STDBOOL_H
#  include <stdbool.h>
# endif /* HAVE_STDBOOL_H */

typedef struct cli_s cli_t;
typedef struct cli_menu_s cli_menu_t;
typedef struct cli_option_s cli_option_t;
typedef struct cli_entry_s cli_entry_t;
typedef void (cli_func_t)(cli_t *);

enum cli_entry_type_e {
    CLI_MENU,
    CLI_OPTION
};
typedef enum cli_entry_type_e cli_entry_type_t;

struct cli_option_s {
    unsigned int ref;
    const char * name;
    cli_func_t * func;
};

struct cli_menu_s {
    unsigned int ref;
    const char * name;
    cli_entry_t * entries;
    cli_entry_t * last;
};

struct cli_s {
    bool run;
    cli_menu_t * root;
    void * pointer;
};

struct cli_entry_s {
    cli_entry_type_t type;
    cli_menu_t * menu;
    cli_option_t * option;
    struct cli_entry_s * next;
};

int cli_init(cli_t * const cli, const char * const name)
    __attribute__((nonnull));
void cli_destroy(cli_t * const cli) __attribute__((nonnull));

cli_menu_t * cli_get_root(const cli_t * const cli)
    __attribute__((nonnull));

void cli_set_pointer(cli_t * const cli, void * const pointer)
    __attribute__((nonnull(1)));
void * cli_get_pointer(const cli_t * const cli)
    __attribute__((nonnull));

void cli_run(cli_t * const cli) __attribute__((nonnull));
void cli_quit(cli_t * const cli) __attribute__((nonnull));

cli_menu_t * cli_menu_new(const char * const name)
    __attribute__((nonnull));
int cli_menu_add_submenu(cli_menu_t * const menu, cli_menu_t * const submenu)
    __attribute__((nonnull));
int cli_menu_add_option(cli_menu_t * const menu, cli_option_t * const option)
    __attribute__((nonnull));

cli_option_t * cli_option_new(const char * const name, cli_func_t * const func)
    __attribute__((nonnull));

int cli_input_float(const char * const prompt, float * const dest)
    __attribute__((nonnull));
int cli_input_int(const char * const prompt, int * const dest)
    __attribute__((nonnull));
char * cli_input_string(const char * const prompt)
    __attribute__((nonnull));
int cli_input_uint(const char * const prompt, unsigned int * const dest)
    __attribute__((nonnull));

/* This function prompts the user to answer a question with either yes
 * or no.
 * Returns -1 on error, 0 on no and 1 on yes. */
int cli_input_yesno(const char * const prompt)
    __attribute__((nonnull));

void cli_print_error(const char * const fmt, ...)
    __attribute__((nonnull));
void cli_print_question(const char * const question)
    __attribute__((nonnull));
void cli_perror(const char * const s);

#endif /* !CLI_H */
