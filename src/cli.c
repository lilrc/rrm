/* 
 * This file is part of rrm.
 * 
 * Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

/* assert() */
#include <assert.h>

/* errno */
#include <errno.h>

/* UINT_MAX */
#if HAVE_LIMITS_H
# include <limits.h>
#endif /* HAVE_LIMITS_H */

/* va_end(), va_list, va_start(), vfprintf */
#include <stdarg.h>

/* false, true */
#if HAVE_STDBOOL_H
# include <stdbool.h>
#endif /* HAVE_STDBOOL_H */

/* perror(), printf(), putchar(), puts() */
#include <stdio.h>

/* free(), malloc(), NULL */
#include <stdlib.h>

/* strerror() */
#include <string.h>

#include "cli.h"

static void __attribute__((nonnull))
cli_option_ref(cli_option_t * const option)
{
	option->ref++;
	return;
}

static void __attribute__((nonnull))
cli_option_unref(cli_option_t * const option)
{
	option->ref--;
	if (option->ref == 0) {
		free(option);
	}
	return;
}

static void __attribute__((nonnull))
cli_menu_ref(cli_menu_t * const menu)
{
	menu->ref++;
	return;
}

static void __attribute__((nonnull))
cli_menu_unref(cli_menu_t * const menu)
{
	cli_entry_t * next = menu->entries;
	while (next != NULL) {
		cli_entry_t * const entry = next;
		next = entry->next;
		assert(entry->type == CLI_MENU || entry->type == CLI_OPTION);
		if (entry->type == CLI_MENU) {
			cli_menu_unref(entry->menu);
		} else {
			cli_option_unref(entry->option);
		}
		
		free(entry);
	}
	menu->ref--;
	if (menu->ref == 0) {
		free(menu);
	}
	return;
}

int __attribute__((nonnull))
cli_init(cli_t * const cli, const char * const name)
{
	cli->root = cli_menu_new(name);
	if (cli->root == NULL) {
		return 1;
	}
	cli_menu_ref(cli->root);

	/* The run member is not set because it will be set in cli_run()
	 * before it is used. */
	/* cli->run = false; */

	cli->pointer = NULL;
	return 0;
}

void __attribute__((nonnull))
cli_destroy(cli_t * const cli)
{
	cli_menu_unref(cli->root);
	return;
}

cli_menu_t * __attribute__((nonnull))
cli_get_root(const cli_t * const cli)
{
	return cli->root;
}

void __attribute__((nonnull(1)))
cli_set_pointer(cli_t * const cli, void * const pointer)
{
	cli->pointer = pointer;
	return;
}

void * __attribute__((nonnull))
cli_get_pointer(const cli_t * const cli)
{
	return cli->pointer;
}

static unsigned short __attribute__((const))
digits(const unsigned int i)
{
	unsigned short n_digits = 1;
	unsigned int power = 10;
	while (i >= power) {
		n_digits += 1;
		power *= 10;
	}
	return n_digits;
}

static void
print_uint(const unsigned int i, const unsigned int max)
{
	assert(max >= i);
	const unsigned short diff = digits(max) - digits(i);
	for (unsigned short j = 0; j < diff; ++j) {
		putchar(' ');
	}
	printf("%d", i);
	return;
}

static void __attribute__((nonnull))
cli_option_run(cli_t * const cli, const cli_option_t * const option)
{
	(*option->func)(cli);
	return;
}

static void
cli_wait_for_enter(void)
{
	fputs("--- Press [ENTER] to continue. ---", stdout);
	while (true) {
		const char c = getchar();
		if (c == '\n' || c == EOF) {
			return;
		}
	}
}

static void __attribute__((nonnull))
cli_menu_run(cli_t * const cli, const cli_menu_t * const menu)
{
	/* 1 for the exit iption. */
	unsigned int n = 1;
	for (const cli_entry_t * entry = menu->entries;
	      entry != NULL;
	      entry = entry->next)
	{
		n++;
	}

	while (cli->run) {
	again:
		printf("=== %s ===\n", menu->name);
		unsigned int i = 1;
		for (const cli_entry_t * entry = menu->entries;
		      entry != NULL;
		      entry = entry->next)
		{
			fputs("::: [", stdout);
			print_uint(i, n);
			fputs("] ", stdout);

			assert(entry->type == CLI_MENU ||
			       entry->type == CLI_OPTION);
			if (entry->type == CLI_MENU) {
				puts(entry->menu->name);
			} else {
				puts(entry->option->name);
			}

			i++;
		}

		/* Print the exit option. */
		fputs("::: [", stdout);
		print_uint(i, n);
		fputs("] Exit\n", stdout);

		int chosen;
		while (true) {
			if (cli_input_int("Choose an option", &chosen)) {
				cli_print_error("Invalid option");
				continue;
			} else if (chosen <= 0) {
				cli_print_error("Invalid option");
				continue;
			}

			int j = 1;
			for (const cli_entry_t * entry = menu->entries;
			      entry != NULL;
			      entry = entry->next)
			{
				if (j == chosen) {
					assert(entry->type == CLI_MENU ||
					       entry->type == CLI_OPTION);
					if (entry->type == CLI_MENU) {
						putchar('\n');
						cli_menu_run(cli, entry->menu);
						putchar('\n');
					} else {
						putchar('\n');
						cli_option_run(cli, entry->option);
						putchar('\n');
						cli_wait_for_enter();
						putchar('\n');
					}
					goto again;
				} else {
					++j;
				}
			}
			if (j == chosen) {
				return;
			} else {
				cli_print_error("Invalid option");
			}
		}
	}
}

void __attribute__((nonnull))
cli_run(cli_t * const cli)
{
	cli->run = true;
	cli_menu_run(cli, cli->root);
	return;
}

void __attribute__((nonnull))
cli_quit(cli_t * const cli)
{
	cli->run = false;
	return;
}

cli_menu_t * __attribute__((nonnull))
cli_menu_new(const char * const name)
{
	cli_menu_t * const menu = malloc(sizeof(cli_menu_t));
	if (menu == NULL) {
		perror("malloc");
		return NULL;
	}

	menu->ref = 0;
	menu->name = name;
	menu->entries = NULL;

	return menu;
}

static cli_entry_t *
cli_entry_new(void)
{
	cli_entry_t * const entry = malloc(sizeof(cli_entry_t));
	if (entry == NULL) {
		perror("malloc");
		return NULL;
	}

	entry->next = NULL;
	return entry;
}

static cli_entry_t * __attribute__((nonnull))
cli_menu_entry_new(cli_menu_t * const menu)
{
	cli_entry_t * const entry = cli_entry_new();
	if (entry == NULL) {
		return NULL;
	}

	entry->type = CLI_MENU;
	entry->menu = menu;

	return entry;
}

static cli_entry_t * __attribute__((nonnull))
cli_option_entry_new(cli_option_t * const option)
{
	cli_entry_t * const entry = cli_entry_new();
	if (entry == NULL) {
		return NULL;
	}

	entry->type = CLI_OPTION;
	entry->option = option;

	return entry;
}

static void __attribute__((nonnull))
cli_menu_add_entry(cli_menu_t * const menu, cli_entry_t * const entry)
{
	if (menu->entries == NULL) {
		menu->entries = entry;
	} else {
		menu->last->next = entry;
	}
	menu->last = entry;
	return;
}

int __attribute__((nonnull))
cli_menu_add_submenu(cli_menu_t * const menu, cli_menu_t * const submenu)
{
	cli_entry_t * const entry = cli_menu_entry_new(submenu);
	if (entry == NULL) {
		return 1;
	}

	cli_menu_ref(submenu);
	cli_menu_add_entry(menu, entry);

	return 0;
}

int __attribute__((nonnull))
cli_menu_add_option(cli_menu_t * const menu, cli_option_t * const option)
{
	cli_entry_t * const entry = cli_option_entry_new(option);
	if (entry == NULL) {
		return 1;
	}

	cli_option_ref(option);
	cli_menu_add_entry(menu, entry);

	return 0;
}

cli_option_t *__attribute__((nonnull))
cli_option_new(const char * const name, cli_func_t * const func)
{
	cli_option_t * const option = malloc(sizeof(cli_option_t));
	if (option == NULL) {
		perror("malloc");
		return NULL;
	}

	option->ref = 0;
	option->name = name;
	option->func = func;

	return option;
}

int __attribute__((nonnull))
cli_input_float(const char * const prompt, float * const dest)
{
	char * const string = cli_input_string(prompt);
	if (string == NULL) {
		return 1;
	}

	char * endptr;
	errno = 0;
	*dest = strtof(string, &endptr);
	free(string);

	if (errno != 0) {
		cli_perror("Conversion error");
		return 1;
	} else if (*dest == 0 && string == endptr) {
		cli_print_error("Conversion error");
		return 1;
	}

	return 0;
}

int __attribute__((nonnull))
cli_input_int(const char * const prompt, int * const dest)
{
	char * const string = cli_input_string(prompt);
	if (string == NULL) {
		return 1;
	}

	*dest = atoi(string);
	free(string);

	return 0;
}

#define INPUT_STRING_CHUNK_SIZE 64
char * __attribute__((nonnull))
cli_input_string(const char * const prompt)
{
	fputs("??? ", stdout);
	puts(prompt);

	fputs(">>> ", stdout);

	size_t string_len = 0;
	size_t string_size = 0;
	char * string = NULL;

	while (true) {
		if (string_len == string_size) {
			string_size += INPUT_STRING_CHUNK_SIZE;
			string = realloc(string, string_size * sizeof(char));
			if (string == NULL) {
				perror("realloc");
				return NULL;
			}
		}

		const char c = getchar();
		if (c == EOF) {
			if (ferror(stdin)) {
				cli_print_error("Input error");
				free(string);
				return NULL;
			} else {
				string[string_len] = '\0';
				break;
			}
		} else if (c == '\n') {
			string[string_len] = '\0';
			break;
		} else {
			string[string_len++] = c;
		}
	}
	string = realloc(string, (string_len + 1) * sizeof(char));
	if (string == NULL) {
		perror("realloc");
		return NULL;
	}

	return string;
}

int __attribute__((nonnull))
cli_input_uint(const char * const prompt, unsigned int * const dest)
{
	char * const string = cli_input_string(prompt);
	if (string == NULL) {
		return 1;
	}

	char * endptr;
	errno = 0;
	unsigned long int uli = strtoul(string, &endptr, 10);
	free(string);

	if (errno != 0) {
		cli_perror("Conversion error");
		return 1;
	} else if (uli == 0 && string == endptr) {
		cli_print_error("Conversion error");
		return 1;
	} else if (uli > UINT_MAX) {
		errno = ERANGE;
		cli_perror("Conversion error");
		return 1;
	}

	*dest = uli;
	return 0;
}

int __attribute__((nonnull))
cli_input_yesno(const char * const prompt)
{
	const char * no_answers[] = {"No", "no", "N", "n", NULL};
	const char * yes_answers[] = {"Yes", "yes", "Y", "y", NULL};

	while (true) {
		char * const string = cli_input_string(prompt);
		if (string == NULL) {
			return -1; /* error */
		}

		for (size_t i = 0; no_answers[i] != NULL; ++i) {
			if (strcmp(string, no_answers[i]) == 0) {
				free(string);
				return 0; /* no */
			}
		}

		for (size_t i = 0; yes_answers[i] != NULL; ++i) {
			if (strcmp(string, yes_answers[i]) == 0) {
				free(string);
				return 1; /* yes */
			}
		}

		cli_print_error("Invalid answer");
		fputs("!!! Valid answers are: ", stderr);

		bool print_slash = false;
		for (size_t i = 0; no_answers[i] != NULL; ++i) {
			if (print_slash) {
				fputc('/', stderr);
			}
			fputs(no_answers[i], stderr);
			print_slash = true;
		}
		for (size_t i = 0; yes_answers[i] != NULL; ++i) {
			if (print_slash) {
				fputc('/', stderr);
			}
			fputs(yes_answers[i], stderr);
			print_slash = true;
		}
		fputc('\n', stderr);

		free(string);
	}
}

void __attribute__((nonnull))
cli_print_error(const char * const fmt, ...)
{
	fputs("!!! ", stderr);

	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	fputc('\n', stderr);
	return;
}

void __attribute__((nonnull))
cli_print_question(const char * const question)
{
	fputs("??? ", stdout);
	puts(question);
	return;
}

void
cli_perror(const char * const s)
{
	fputs("!!! ", stderr);
	if (s != NULL) {
		fputs(s, stderr);
		fputs(": ", stderr);
	}
	fputs(strerror(errno), stderr);
	fputc('\n', stderr);
	return;
}
