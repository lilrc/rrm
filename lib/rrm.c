/*
 * This file is part of rrm.
 *
 * Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

/* assert() */
#include <assert.h>

/* EINVAL, errno */
#include <errno.h>

/* bool, false, true */
#if HAVE_STDBOOL_H
# include <stdbool.h>
#endif /* HAVE_STDBOOL_H */

/* free(), malloc(), NULL, size_t */
#include <stdlib.h>

/* uint8_t */
#include <stdint.h>

/* FILE, perror() */
#include <stdio.h>

/* memcpy(), strlen() */
#include <string.h>

/* localtime(), time(), time_t */
#include <time.h>

#include <rrm.h>

/* The same as strlen("amount") just defined here to avoid some
 * unnecessary calls to strlen. */
#define STRLEN_AMOUNT 6

/* The same as strlen("total") just defined here to avoid some
 * unnecessary calls to strlen. */
#define STRLEN_TOTAL 5

/* A type that is used to determine whether to load a receipt or a
 * repayment from a file. */
typedef uint8_t rrm_type_t;

/* No type. Used in rrm_type_read() to indicate EOF. */
#define RRM_TYPE_NONE      0

/* No type. Used in rrm_type_read() to indicate error. */
#define RRM_TYPE_ERROR     1

/* These are used in the file to indicate whether what kind of data a
 * part of the file consists of. */
#define RRM_TYPE_RECEIPT   2
#define RRM_TYPE_REPAYMENT 3

struct rrm_node_s {
    rrm_receipt_t * receipt;
    struct rrm_node_s * next;
};

struct rrm_repayment_s {
    unsigned int id;

    bool saved;
    long position;

    rrm_list_t receipts;
    float amount;

    /* Time of creation. */
    time_t time;

    struct rrm_repayment_s * next;
};

#define RECEIPT_NAME_MAX 48
struct rrm_receipt_s {
    /* Set to true if the receipt is in sync with the file, otherwise
     * false. */
    bool saved;

    /* Unique identifier. */
    unsigned int id;

    /* Position in file. If it is in the file, otherwise it is -1. */
    long position;

    /* Reference count. */
    unsigned int ref;

    /* The repayment at which the receipt was repayed or NULL if the
     * receipt has not been repayed yet. */
    rrm_repayment_t * repayment;

    /* Time of creation. */
    time_t time;

    float amount;
    char name[RECEIPT_NAME_MAX];
    bool removed;
};

struct rrm_s {
    FILE * file;

    /* Set to true if there is unsaved content, otherwise false. */
    bool saved;

    /* The maximum id in this receipt file. */
    unsigned int id_max;

    /* This simplifies receipt printing. */
    float amount_sum;
    float amount_max;
    unsigned short name_len_max;

    rrm_list_t all;
    rrm_list_t receipts;
    rrm_list_t repayed;
    rrm_list_t removed;

    float repayment_amount_max;
    rrm_repayment_t * repayments;
    rrm_repayment_t * last_repayment;
};

static void rrm_repayment_print_date(const rrm_repayment_t * const repayment)
    __attribute__((nonnull));

static void __attribute__((nonnull))
rrm_receipt_destroy(rrm_receipt_t * const receipt)
{
    assert(receipt->ref == 0);
    free(receipt);
    return;
}

static void __attribute__((nonnull))
rrm_receipt_ref(rrm_receipt_t * const receipt)
{
    receipt->ref++;
    return;
}

static void __attribute__((nonnull))
rrm_receipt_unref(rrm_receipt_t * const receipt)
{
    receipt->ref--;
    if (receipt->ref == 0) {
        rrm_receipt_destroy(receipt);
    }
    return;
}

/* Assigns a receipt to the node unassigning the previous one. */
inline static void __attribute__((nonnull))
rrm_node_reassign(rrm_node_t * const node,
                  rrm_receipt_t * const receipt)
{
    rrm_receipt_unref(node->receipt);
    rrm_receipt_ref(receipt);
    node->receipt = receipt;
    return;
}

static rrm_node_t * __attribute__((nonnull))
rrm_node_new(rrm_receipt_t * const receipt)
{
    rrm_node_t * const node = malloc(sizeof(rrm_node_t));
    if (node == NULL) {
        return NULL;
    }

    rrm_receipt_ref(receipt);
    node->receipt = receipt;

    /* node->next is set right before the node is linked into the list
     * so there is no need setting it here. */
    /* node->next = NULL; */

    return node;
}

static void __attribute__((nonnull))
rrm_node_destroy(rrm_node_t * const node)
{
    rrm_receipt_unref(node->receipt);
    free(node);
    return;
}

inline rrm_node_t * __attribute__((nonnull))
rrm_node_next(const rrm_node_t * const node)
{
    return node->next;
}

inline rrm_receipt_t * __attribute__((nonnull))
rrm_node_receipt(const rrm_node_t * const node)
{
    return node->receipt;
}

static inline void __attribute__((nonnull))
rrm_node_link(rrm_node_t * const prev, rrm_node_t * const node)
{
    node->next = prev->next;
    prev->next = node;
    return;
}

inline void __attribute__((nonnull))
rrm_list_init(rrm_list_t * const list)
{
    list->first = NULL;
    /* The last member is not read if the list member is NULL. */
    /* list->last = NULL; */
    return;
}

void __attribute__((nonnull))
rrm_list_destroy(rrm_list_t * const list)
{
    rrm_node_t * next = rrm_list_first(list);
    while (next != NULL) {
        rrm_node_t * const node = next;
        next = rrm_node_next(node);
        rrm_node_destroy(node);
    }
    return;
}

inline rrm_node_t * __attribute__((nonnull))
rrm_list_first(const rrm_list_t * const list)
{
    return list->first;
}

static void __attribute__((nonnull))
rrm_list_link_first(rrm_list_t * const list,
                    rrm_node_t * const node)
{
    if (list->first == NULL) {
        list->last = node;
    }
    node->next = list->first;
    list->first = node;
    return;
}

static void __attribute__((nonnull))
rrm_list_link_last (rrm_list_t * const list,
                     rrm_node_t * const node)
{
    if (rrm_list_first(list) == NULL) {
        rrm_list_link_first(list, node);
    } else {
        rrm_node_link(list->last, node);
        list->last = node;
    }
    return;
}

static int __attribute__((nonnull))
rrm_list_append(rrm_list_t * const list,
                rrm_receipt_t * const receipt)
{
    rrm_node_t * const node = rrm_node_new(receipt);
    if (node == NULL) {
        return 1;
    }

    rrm_list_link_last(list, node);

    return 0;
}

/* Extends the list with a chain of nodes. */
static void __attribute__((nonnull))
rrm_list_extend(rrm_list_t * const list,
                rrm_node_t * const node)
{
    /* Find the last node in the chain. */
    rrm_node_t * last = node;
    rrm_node_t * next;
    while ((next = rrm_node_next(last)) != NULL) {
        last = next;
    }

    /* Link the node with the list and update the last memeber. */
    if (list->first == NULL) {
        list->first = node;
    } else {
        list->last->next = node;
    }
    list->last = last;

    return;
}

/* Pops the first node from the list. Returns NULL if there are no more
 * nodes. */
static rrm_node_t * __attribute__((nonnull))
rrm_list_pop(rrm_list_t * const list)
{
    rrm_node_t * const first = rrm_list_first(list);
    if (first != NULL) {
        list->first = rrm_node_next(first);
    }
    return first;
}

/* Unlinks a receipt from the list returning the containing node. */
static rrm_node_t * __attribute__((nonnull))
rrm_list_unlink_from_receipt(rrm_list_t * const list,
                             rrm_receipt_t * const receipt)
{
    rrm_node_t * prev = rrm_list_first(list);
    assert(prev != NULL);

    if (rrm_node_receipt(prev) == receipt) {
        list->first = rrm_node_next(prev);
        return prev;
    } else {
        rrm_node_t * node = rrm_node_next(prev);
        while (node != NULL) {
            if (rrm_node_receipt(node) != receipt) {
                prev = node;
                node = rrm_node_next(node);
                continue;
            } else {
                prev->next = rrm_node_next(node);
                if (node == list->last) {
                    list->last = prev;
                }
                return node;
            }
        }
        return NULL;
    }
}

static void __attribute__((nonnull))
rrm_list_remove(rrm_list_t * const list,
                rrm_receipt_t * const receipt)
{
    rrm_node_t * const node =
        rrm_list_unlink_from_receipt(list, receipt);
    if (node != NULL) {
        rrm_node_destroy(node);
    }
    return;
}

void __attribute__((nonnull))
rrm_list_print(const rrm_t * const rrm,
               const rrm_list_t * const list)
{
    rrm_receipt_header_print(rrm);
    for (const rrm_node_t * node = list->first;
          node != NULL;
          node = rrm_node_next(node))
    {
        rrm_receipt_print(rrm, node->receipt);
    }
    return;
}

static rrm_type_t __attribute__((nonnull))
rrm_type_read(const rrm_t * const rrm)
{
    rrm_type_t type;
    const int r = fread(&type, sizeof(rrm_type_t), 1, rrm->file);
    if (r == 0) {
        if (ferror(rrm->file)) {
            return RRM_TYPE_ERROR;
        }
        return RRM_TYPE_NONE;
    }
    return type;
}

static int __attribute__((nonnull))
rrm_type_write(const rrm_t * const rrm, const rrm_type_t type)
{
    const int w = fwrite(&type, sizeof(rrm_type_t), 1, rrm->file);
    if (w == 1) {
        return 0;
    } else {
        return 1;
    }
}

rrm_receipt_t * __attribute__((nonnull))
rrm_receipt_new(rrm_t * const rrm,
                const char * const name,
                const float amount)
{
    assert(amount > 0);

    rrm_receipt_t * const receipt = malloc(sizeof(rrm_receipt_t));
    if (receipt == NULL) {
        return NULL;
    }

    receipt->time = time(NULL);
    receipt->saved = false;
    receipt->ref = 0;
    receipt->id = rrm->id_max + 1;
    receipt->position = -1;

    memset(receipt->name, 0, RECEIPT_NAME_MAX * sizeof(char));
    size_t len = strlen(name);
    if (len >= RECEIPT_NAME_MAX) {
        len = RECEIPT_NAME_MAX - 1;
    }
    memcpy(receipt->name, name, len * sizeof(char));

    receipt->repayment = NULL;
    receipt->amount = amount;
    receipt->removed = false;

    if (rrm_list_append(&rrm->all, receipt)) {
        goto error;
    }
    if (rrm_list_append(&rrm->receipts, receipt)) {
        rrm_list_remove(&rrm->all, receipt);
        goto error;
    }

    /* Update rrm. */
    if (len > rrm->name_len_max) {
        rrm->name_len_max = len;
    }
    if (amount > rrm->amount_max) {
        rrm->amount_max = amount;
    }
    rrm->id_max++;
    rrm->amount_sum += amount;
    rrm->saved = false;

    return receipt;
error:
    rrm_receipt_destroy(receipt);
    return NULL;
}

int __attribute__((nonnull, pure))
rrm_receipt_is_removed(const rrm_receipt_t * const receipt)
{
    return receipt->removed ? 1 : 0;
}

int __attribute__((nonnull, pure))
rrm_receipt_is_repayed(const rrm_receipt_t * const receipt)
{
    return receipt->repayment != NULL ? 1 : 0;
}

/* Returns 0 on success and 1 on failure. */
static int __attribute__((nonnull))
rrm_receipt_read(rrm_t * const rrm)
{
    rrm_receipt_t * const receipt = malloc(sizeof(rrm_receipt_t));
    if (receipt == NULL) {
        return 1;
    }

    receipt->saved = true;
    receipt->ref = 0;
    receipt->id = rrm->id_max + 1;
    receipt->repayment = NULL;

    receipt->position = ftell(rrm->file);
    if (receipt->position < 0) {
        free(receipt);
        return -1;
    }

    size_t r;
    r = fread(&receipt->amount, sizeof(float), 1, rrm->file);
    if (r != 1) {
        goto error;
    }
    assert(receipt->amount > 0);

    r = fread(&receipt->name, sizeof(char), RECEIPT_NAME_MAX,
              rrm->file);
    if (r != RECEIPT_NAME_MAX) {
        goto error;
    }

    r = fread(&receipt->time, sizeof(time_t), 1, rrm->file);
    if (r != 1) {
        goto error;
    }

    r = fread(&receipt->removed, sizeof(bool), 1, rrm->file);
    if (r != 1) {
        goto error;
    }
    assert(receipt->removed == true || receipt->removed == false);

    /* Add the receipt to all the lists. */
    if (rrm_list_append(&rrm->all, receipt)) {
        goto error;
    }
    if (!receipt->removed) {
        rrm->amount_sum += receipt->amount;
        if (rrm_list_append(&rrm->receipts, receipt)) {
            rrm_list_remove(&rrm->all, receipt);
            goto error;
        }
    } else {
        if (rrm_list_append(&rrm->removed, receipt)) {
            rrm_list_remove(&rrm->all, receipt);
            goto error;
        }
    }

    const size_t len = strlen(receipt->name);
    if (len > rrm->name_len_max) {
        rrm->name_len_max = len;
    }
    if (receipt->amount > rrm->amount_max) {
        rrm->amount_max = receipt->amount;
    }
    rrm->id_max++;
    return 0;
error:
    rrm_receipt_destroy(receipt);
    return 1;
}

static int __attribute__((nonnull))
rrm_receipt_write(rrm_t * const rrm, rrm_receipt_t * const receipt)
{
    /* Avoid unnecessary writes and seeks in the file. */
    if (receipt->saved) return 0;

    /* Seek the file to the right place. */
    if (receipt->position < 0) {
        if (fseek(rrm->file, 0L, SEEK_END)) {
            return 1;
        }

        /* Write the type indicator. */
        if (rrm_type_write(rrm, RRM_TYPE_RECEIPT)) {
            return 1;
        }

        receipt->position = ftell(rrm->file);
        if (receipt->position < 0) {
            return 1;
        }
    } else {
        if (fseek(rrm->file, receipt->position, SEEK_SET)) {
            return 1;
        }
    }

    size_t w;
    w = fwrite(&receipt->amount, sizeof(float), 1, rrm->file);
    assert(w == 1);

    w = fwrite(&receipt->name, sizeof(char), RECEIPT_NAME_MAX,
               rrm->file);
    assert(w == RECEIPT_NAME_MAX);

    w = fwrite(&receipt->time, sizeof(time_t), 1, rrm->file);
    assert(w == 1);

    w = fwrite(&receipt->removed, sizeof(bool), 1, rrm->file);
    assert(w == 1);

    receipt->saved = true;
    return 0;
}

rrm_receipt_t * __attribute__((nonnull))
rrm_receipt_get_from_id(const rrm_t * const rrm, const unsigned int id)
{
    if (id == 0) {
        goto error;
    }

    rrm_node_t * node = rrm_list_first(&rrm->all);
    if (node != NULL) {
        for (unsigned int j = 1; j < id; ++j) {
            node = rrm_node_next(node);
            if (node == NULL) {
                goto error;
            }
        }
        assert(node->receipt->id == id);
        return node->receipt;
    } else {
    error:
        errno = EINVAL;
        return NULL;
    }
}

int __attribute__((nonnull))
rrm_receipt_remove_from_id(rrm_t * const rrm, const unsigned int id)
{
    rrm_node_t * node = NULL;
    rrm_node_t * prev = NULL;

    if (id == 0)
    {
        goto error;
    }

    node = rrm_list_first(&rrm->receipts);
    if (!node)
    {
        goto error;
    }

    while (node && node->receipt->id != id) {
        prev = node;
        node = rrm_node_next(node);
    }
    if (!node)
    {
        goto error;
    }

    /* Unlink the found node. */
    if (prev)
    {
        prev->next = rrm_node_next(node);
    }
    else
    {
        rrm->receipts.first = rrm_node_next(node);
    }
    if (node == rrm->receipts.last)
    {
        rrm->receipts.last = prev;
    }

    rrm_list_link_last(&rrm->removed, node);

    node->receipt->removed = true;
    node->receipt->saved = false;
    rrm->amount_sum -= node->receipt->amount;
    rrm->saved = false;
    return 0;
error:
    errno = EINVAL;
    return 1;
}

static unsigned short __attribute__((const))
digits(const unsigned int i)
{
    unsigned short n_digits = 1;
    unsigned int power = 10;
    while (i >= power) {
        n_digits += 1;
        power *= 10;
    }
    return n_digits;
}

static short __attribute__((nonnull))
rrm_amount_col_width(const rrm_t * const rrm)
{
    const float max = (rrm->amount_sum > rrm->amount_max) ?
        rrm->amount_sum : rrm->amount_max;
    const unsigned short width = digits(max) + 3;
    if (width <= STRLEN_AMOUNT) {
        return STRLEN_AMOUNT;
    } else {
        return width;
    }
}

void __attribute__((nonnull))
rrm_receipt_header_print(const rrm_t * const rrm)
{
    const unsigned short amount_width = rrm_amount_col_width(rrm);
    const unsigned short id_digits = digits(rrm->id_max);

    fputs(" ID", stdout);
    for (unsigned short i = 2; i < 2 * id_digits + 1; ++i) {
        putchar(' ');
    }

    fputs("   Removed   Repayed      Name", stdout);
    /*                  YYYY-MM-DD */

    for (unsigned short i = 4; i < rrm->name_len_max; ++i) {
        putchar(' ');
    }
    fputs("   ", stdout);
    for (unsigned short i = 0;
          i < amount_width - STRLEN_AMOUNT;
          ++i)
    {
        putchar(' ');
    }
    puts("Amount");
    return;
}

void __attribute__((nonnull))
rrm_receipt_print(const rrm_t * const rrm, const rrm_receipt_t * const receipt)
{
    const unsigned short amount_width_total = rrm_amount_col_width(rrm);
    const unsigned short id_digits_max = digits(rrm->id_max);
    const unsigned short amount_width = digits(receipt->amount) + 3;
    const unsigned short id_digits = digits(receipt->id);

    /* Common indentation. */
    putchar(' ');

    /* ID */
    for (unsigned short i = 0; i < id_digits_max - id_digits; ++i) {
        putchar(' ');
    }
    printf("%d/%d   ", receipt->id, rrm->id_max);

    /* Removed */
    if (receipt->removed) {
        fputs("   *      ", stdout);
    } else {
        fputs("          ", stdout);
    }

    /* Repayed. */
    if (receipt->repayment != NULL) {
        rrm_repayment_print_date(receipt->repayment);
        fputs("   ", stdout);
    } else {
        /*     YYYY-MM-DD */
        fputs("             ", stdout);
    }

    /* Name */
    const size_t name_len = printf("%s", receipt->name);
    for (unsigned short i = 0; i < rrm->name_len_max - name_len; ++i) {
        putchar(' ');
    }
    fputs("   ", stdout);

    /* Amount. */
    for (unsigned short i = 0;
          i < amount_width_total - amount_width;
          ++i)
    {
        putchar(' ');
    }
    printf("%.2f\n", receipt->amount);
    return;
}

void __attribute__((nonnull))
rrm_print_total(const rrm_t * const rrm, const float total)
{
    /* This is needed avoid printing the sign if the total is zero. */
    if (total < 0 && rrm_amount_is_eq(total, 0)) {
        rrm_print_total(rrm, 0);
        return;
    }

    const unsigned short amount_width_total = rrm_amount_col_width(rrm);
    const unsigned short id_digits = digits(rrm->id_max);
    const unsigned short amount_width = digits(total) + 3;

    putchar(' ');
    for (unsigned short i = 0; i < 2 * id_digits + 1; ++i) {
        putchar(' ');
    }

    /*    |   Removed   Repayed      Name
     *    |             YYYY-MM-DD */
    fputs("                          Total   ", stdout);
    for (unsigned short i = STRLEN_TOTAL; i < rrm->name_len_max; ++i)
    {
        putchar(' ');
    }
    for (unsigned short i = 0;
          i < amount_width_total - amount_width;
          ++i)
    {
        putchar(' ');
    }
    printf("%.2f\n", total);
    return;
}

void __attribute__((nonnull))
rrm_receipt_print_active(const rrm_t * const rrm)
{
    if (rrm->id_max > 0) {
        rrm_list_print(rrm, &rrm->receipts);
        rrm_print_total(rrm, rrm->amount_sum);
    } else {
        puts("No receipts to print");
    }
    return;
}


void __attribute__((nonnull))
rrm_receipt_print_all(const rrm_t * const rrm)
{
    if (rrm->id_max > 0) {
        rrm_list_print(rrm, &rrm->all);
        rrm_print_total(rrm, rrm->amount_sum);
    } else {
        puts("No receipts to print");
    }
    return;
}

static rrm_repayment_t * __attribute__((nonnull))
rrm_repayment_new(rrm_t * const rrm)
{
    rrm_repayment_t * const repayment = malloc(sizeof(rrm_repayment_t));
    if (repayment == NULL) {
        return NULL;
    }

    repayment->amount = 0;
    repayment->next = NULL;
    if (rrm->repayments == NULL) {
        repayment->id = 1;
        rrm->repayments = repayment;
    } else {
        repayment->id = rrm->last_repayment->id + 1;
        rrm->last_repayment->next = repayment;
    }
    rrm->last_repayment = repayment;

    rrm_list_init(&repayment->receipts);

    /* The creation date will be filled in by the caller.
     * repayment->saved and repayment->position will also be set by the
     * caller. */

    return repayment;
}

static void __attribute__((nonnull))
rrm_repayment_destroy(rrm_t * const rrm, rrm_repayment_t * const repayment)
{
    if (repayment == rrm->repayments) {
        rrm->repayments = repayment->next;
    } else {
        rrm_repayment_t * prev = rrm->repayments;
        while (prev != NULL && prev->next != repayment) {
            prev = prev->next;
        }
        if (prev == NULL) {
            /* Not found. */
            return;
        }
        prev->next = repayment->next;
        if (repayment == rrm->last_repayment) {
            rrm->last_repayment = prev;
        }
    }
    rrm_list_destroy(&repayment->receipts);
    free(repayment);
    return;
}

rrm_repayment_t * __attribute__((nonnull))
rrm_repayment_create (rrm_t * const rrm)
{
    rrm_repayment_t * const repayment = rrm_repayment_new(rrm);
    if (repayment == NULL) {
        return NULL;
    }

    repayment->time = time(NULL);
    repayment->position = -1;
    repayment->saved = false;

    rrm->saved = false;
    return repayment;
}

rrm_error_t __attribute__((nonnull))
rrm_repayment_add_receipt(rrm_t * const rrm,
                          rrm_repayment_t * const repayment,
                          rrm_receipt_t * const receipt)
{
    if (receipt->removed) {
        return RRM_ERROR_REMOVED;
    }

    /* Don't allow receipts newer than the repayment to be added. */
    if (receipt->time > repayment->time) {
        return RRM_ERROR_TIME;
    }

    if (rrm_list_append(&repayment->receipts, receipt)) {
        return RRM_ERROR_ERRNO;
    }

    rrm_node_t * const node =
        rrm_list_unlink_from_receipt(&rrm->receipts, receipt);
    if (node == NULL) {
        rrm_list_remove(&repayment->receipts, receipt);
        return RRM_ERROR_REPAYED;
    }

    rrm_list_link_last(&rrm->repayed, node);
    receipt->repayment = repayment;
    repayment->amount += receipt->amount;
    rrm->amount_sum -= receipt->amount;
    if (repayment->amount > rrm->repayment_amount_max) {
        rrm->repayment_amount_max = repayment->amount;
    }

    repayment->saved = false;
    rrm->saved = false;
    return RRM_ERROR_NONE;
}

static int __attribute__((nonnull))
rrm_repayment_read(rrm_t * const rrm)
{
    rrm_repayment_t * const repayment = rrm_repayment_new(rrm);
    if (repayment == NULL) {
        return 1;
    }

    repayment->position = ftell(rrm->file);
    if (repayment->position < 0) {
        goto error;
    }

    size_t r;
    r = fread(&repayment->time, sizeof(time_t), 1, rrm->file);
    if (r != 1) {
        goto error;
    }

    while (true) {
        unsigned int id;
        r = fread(&id, sizeof(unsigned int), 1, rrm->file);
        if (r != 1) {
            goto error;
        }

        if (id == 0) break;

        rrm_receipt_t * const receipt =
            rrm_receipt_get_from_id(rrm, id);
        if (receipt == NULL) {
            /* Not found. This should never happen. */
            goto error;
        }

        if (rrm_repayment_add_receipt(rrm, repayment, receipt)) {
            goto error;
        }
    }

    repayment->saved = true;
    return 0;
error:
    rrm_repayment_destroy(rrm, repayment);
    return 1;
}

static int __attribute__((nonnull))
rrm_repayment_write(const rrm_t * const rrm,
                    rrm_repayment_t * const repayment)
{
    /* Avoid unnecessary writes and seeks in the file. */
    if (repayment->saved) return 0;

    /* Seek the file to the right place. */
    if (repayment->position < 0) {
        if (fseek(rrm->file, 0L, SEEK_END)) {
            return 1;
        }

        /* Write the type indicator. */
        if (rrm_type_write(rrm, RRM_TYPE_REPAYMENT)) {
            return 1;
        }

        repayment->position = ftell(rrm->file);
        if (repayment->position < 0) {
            return 1;
        }
    } else {
        if (fseek(rrm->file, repayment->position, SEEK_SET)) {
            return 1;
        }
    }

    size_t w;
    w = fwrite(&repayment->time, sizeof(time_t), 1, rrm->file);
    assert(w == 1);

    for (const rrm_node_t * node =
            rrm_list_first(&repayment->receipts);
          node != NULL;
          node = rrm_node_next(node))
    {
        const unsigned int id = node->receipt->id;
        w = fwrite(&id, sizeof(unsigned int), 1, rrm->file);
        assert(w == 1);
    }

    /* Write the end of the receipt ID list (0) */
    const unsigned int end = 0;
    w = fwrite(&end, sizeof(unsigned int), 1, rrm->file);
    assert(w == 1);

    repayment->saved = true;
    return 0;
}

rrm_repayment_t * __attribute__((nonnull))
rrm_repayment_get_from_id(const rrm_t * const rrm, const unsigned int id)
{
    if (id == 0) {
        goto error;
    }

    rrm_repayment_t * repayment = rrm->repayments;
    if (repayment == NULL) {
        goto error;
    }
    for (unsigned int j = 1; j < id; ++j) {
        if (repayment->next != NULL) {
            repayment = repayment->next;
        } else {
            goto error;
        }
    }
    assert(repayment->id == id);
    return repayment;
error:
    errno = EINVAL;
    return NULL;
}

void __attribute__((nonnull))
rrm_repayment_header_print(const rrm_t * const rrm)
{
    const unsigned int id_max = rrm->last_repayment->id;
    const unsigned short id_digits = digits(id_max);
    unsigned short amount_width_total =
        digits(rrm->repayment_amount_max) + 3;
    if (amount_width_total < STRLEN_AMOUNT) {
        amount_width_total = STRLEN_AMOUNT;
    }

    fputs(" ID   ", stdout);
    for (unsigned short i = 2; i < 2 * id_digits + 1; ++i) {
        putchar(' ');
    }
    /*      YYYY-MM-DD */
    printf("Date         ");
    for (unsigned short i = 0;
          i < amount_width_total - STRLEN_AMOUNT;
          ++i)
    {
        putchar(' ');
    }
    puts("Amount");

    return;
}

int __attribute__((nonnull))
rrm_repayment_date(const rrm_repayment_t * const repayment,
                   int * const year,
                   int * const month,
                   int * const day)
{
    const struct tm * const tm = localtime(&repayment->time);
    if (tm == NULL) return 1;

    *year = tm->tm_year + 1900;
    *month = tm->tm_mon + 1;
    *day = tm->tm_mday;
    return 0;
}

static void __attribute__((nonnull))
rrm_repayment_print_date(const rrm_repayment_t * const repayment)
{
    int year, month, day;
    if (rrm_repayment_date(repayment, &year, &month, &day)) {
        /* Error. */
        fputs("\?\?\?\?-\?\?-\?\?", stdout);
    } else {
        printf("%4d-%02d-%02d", year, month, day);
    }
    return;
}

void __attribute__((nonnull))
rrm_repayment_print(const rrm_t * const rrm,
                    const rrm_repayment_t * const repayment)
{
    const unsigned int id_max = rrm->last_repayment->id;
    const unsigned short id_digits = digits(repayment->id);
    const unsigned short id_max_digits = digits(id_max);
    const unsigned short amount_width = digits(repayment->amount) + 3;
    unsigned short amount_width_total =
        digits(rrm->repayment_amount_max) + 3;
    if (amount_width_total < STRLEN_AMOUNT) {
        amount_width_total = STRLEN_AMOUNT;
    }

    /* Common indentation. */
    putchar(' ');

    /* ID */
    for (unsigned short i = 0; i < id_max_digits - id_digits; ++i) {
        putchar(' ');
    }
    printf("%d/%d   ", repayment->id, id_max);

    /* Date */
    rrm_repayment_print_date(repayment);
    fputs("   ", stdout);

    /* Amount. */
    for (unsigned short i = 0;
          i < amount_width_total - amount_width;
          ++i)
    {
        putchar(' ');
    }
    printf("%.2f\n", repayment->amount);

    return;
}

void __attribute__((nonnull))
rrm_repayment_print_all(const rrm_t * const rrm)
{
    if (rrm->repayments != NULL) {
        rrm_repayment_header_print(rrm);
        for (const rrm_repayment_t * repayment = rrm->repayments;
              repayment != NULL;
              repayment = repayment->next)
        {
            rrm_repayment_print(rrm, repayment);
        }
    } else {
        puts("No repayments to print");
    }
    return;
}

static rrm_t *
rrm_new(void)
{
    rrm_t * const rrm = malloc(sizeof(rrm_t));
    if (rrm == NULL) {
        return NULL;
    }

    rrm->file = NULL;
    rrm->saved = true;
    rrm->id_max = 0;
    rrm->amount_sum = 0;
    rrm->amount_max = 0;
    rrm->name_len_max = STRLEN_TOTAL;
    rrm_list_init(&rrm->all);
    rrm_list_init(&rrm->receipts);
    rrm_list_init(&rrm->repayed);
    rrm_list_init(&rrm->removed);

    rrm->repayment_amount_max = 0;
    rrm->repayments = NULL;
    /* rrm->last_repayment does not need to be set because it will never
     * be read if repayments are NULL. */
    /* rrm->last_repayment = NULL; */

    return rrm;
}

rrm_t * __attribute__((nonnull))
rrm_create(const char * const filename)
{
    rrm_t * const rrm = rrm_new();
    if (rrm == NULL) return NULL;

    rrm->file = fopen(filename, "w");
    if (rrm->file == NULL) {
        rrm_close(rrm);
        return NULL;
    }

    return rrm;
}

static int __attribute__((nonnull))
rrm_load(rrm_t * const rrm)
{
    while (true) {
        const rrm_type_t type = rrm_type_read(rrm);
        switch (type) {
            case RRM_TYPE_RECEIPT:
                if (rrm_receipt_read(rrm)) {
                    return 1;
                }
                break;
            case RRM_TYPE_REPAYMENT:
                if (rrm_repayment_read(rrm)) {
                    return 1;
                }
                break;
            case RRM_TYPE_NONE:
                rrm->saved = true;
                return 0;
            default:
                /* Error or invalid return value. */
                return 1;
        }
    }
}

rrm_t * __attribute__((nonnull))
rrm_open(const char * const filename)
{
    rrm_t * const rrm = rrm_new();
    if (rrm == NULL) return NULL;

    rrm->file = fopen(filename, "r+");
    if (rrm->file == NULL) {
        rrm_close(rrm);
        return NULL;
    }

    if (rrm_load(rrm)) {
        rrm_close(rrm);
        return NULL;
    }

    return rrm;
}

void
rrm_close(rrm_t * const rrm)
{
    if (rrm == NULL) return;

    if (rrm->file != NULL) {
        fclose(rrm->file);
    }
    rrm_list_destroy(&rrm->all);
    rrm_list_destroy(&rrm->receipts);
    rrm_list_destroy(&rrm->repayed);
    rrm_list_destroy(&rrm->removed);

    while (rrm->repayments != NULL) {
        rrm_repayment_destroy(rrm, rrm->repayments);
    }

    free(rrm);

    return;
}

int __attribute__((nonnull))
rrm_save(rrm_t * const rrm)
{
    /* Only perceed saving the file if there is unsaved content. */
    if (rrm->saved) return 0;

    /* Receipts must be written first. */
    for (rrm_node_t * node = rrm_list_first(&rrm->all);
          node != NULL;
          node = rrm_node_next(node))
    {
        if (rrm_receipt_write(rrm, node->receipt)) {
            return 1;
        }
    }

    for (rrm_repayment_t * repayment = rrm->repayments;
          repayment != NULL;
          repayment = repayment->next)
    {
        if (rrm_repayment_write(rrm, repayment)) {
            return 1;
        }
    }

    rrm->saved = true;
    return 0;
}

int __attribute__((nonnull))
rrm_is_saved(const rrm_t * const rrm)
{
    return rrm->saved ? 1 : 0;
}

const char *
rrm_strerror(const rrm_error_t error)
{
    switch (error) {
        case RRM_ERROR_NONE:
            return "No error";
        case RRM_ERROR_ERRNO:
            return "Error defined by errno";
        case RRM_ERROR_REMOVED:
            return "Receipt removed";
        case RRM_ERROR_REPAYED:
            return "Receipt already repayed";
        case RRM_ERROR_TIME:
            return "Receipt is newer than repayment";
        default:
            return "Invalid error";
    }
}

int __attribute__((const))
rrm_amount_is_eq(const float amount1, const float amount2)
{
    const float diff = amount1 - amount2;
    if (diff > -.01 && diff < .01) {
        return 1;
    } else {
        return 0;
    }
}

/* Returns NULL on error. */
static rrm_node_t * __attribute__((nonnull))
rrm_fit_recursion(const rrm_node_t * src_node,
                  rrm_list_t * const free_nodes,
                  float * const return_amount,
                  const float target)
{
    rrm_receipt_t * src_receipt = rrm_node_receipt(src_node);
    float best_amount = src_receipt->amount;

    rrm_node_t * best = rrm_list_pop(free_nodes);
    if (best != NULL) {
        rrm_node_reassign(best, src_receipt);
    } else /* if (best == NULL) */ {
        best = rrm_node_new(src_receipt);
        if (best == NULL) {
            /* No cleanup needed. Just return. */
            return NULL;
        }
    }

    /* node must not be a chain except between a recursive call and the
     * possible subsequent swap. */
    rrm_node_t * node = rrm_list_pop(free_nodes);
    if (node != NULL) {
        rrm_node_reassign(node, src_receipt);
    } else {
        node = rrm_node_new(src_receipt);
        if (node == NULL) {
            goto error;
        }
    }

    /* best->next and node->next are by implementation initially
     * undefined so make sure they are defined! */
    best->next = NULL;
    node->next = NULL;

    while (true) {
        float amount = src_receipt->amount;

        if (amount < target) {
            const rrm_node_t * const src_next = rrm_node_next(src_node);
            if (src_next != NULL) {
                float ret;
                rrm_node_t * const next =
                    rrm_fit_recursion(src_next, free_nodes, &ret,
                                      target - amount);
                if (next == NULL) {
                    goto error;
                }
                amount += ret;

                if ((best_amount >= target &&
                       amount >= target &&
                       amount <= best_amount) ||
                     (best_amount < target &&
                       amount >= best_amount))
                {
                    node->next = next;
                    goto swap;
                }

                rrm_list_extend(free_nodes, next);
            } else /* if (src_next == NULL) */ {
                if (best_amount < target && amount >= best_amount) {
                    goto swap;
                }
            }
        } else if (amount == target) {
            goto swap;
        } else /* if (amount > target) */ {
            /* All source nodes after this are guaranteed to have an
             * amount more equal to the current so there is no reason
             * continuing. */
            if (amount < best_amount) {
                rrm_list_extend(free_nodes, best);
                *return_amount = amount;
                return node;
            } else {
                goto end;
            }
        }

        if (false) {
        swap:
            best_amount = amount;
            if (best->next != NULL) {
                rrm_list_extend(free_nodes, best->next);
                best->next = NULL;
            }
            rrm_node_t * const old_best = best;
            best = node;
            node = old_best;
        }

        src_node = rrm_node_next(src_node);
        if (src_node != NULL) {
            src_receipt = rrm_node_receipt(src_node);
            rrm_node_reassign(node, src_receipt);
        } else {
        end:
            rrm_list_link_last(free_nodes, node);
            *return_amount = best_amount;
            return best;
        }
    }

error:
    rrm_list_extend(free_nodes, best);
    if (node != NULL) {
        rrm_list_link_last(free_nodes, node);
    }
    return NULL;
}

/* Helper function to rrm_fit_repayment() creates a sorted copy of the
 * list.
 * Returns 1 on failure. */
static int __attribute__((nonnull))
rrm_list_copy_sorted(const rrm_list_t * const list, rrm_list_t * const sorted)
{
    /* Copy the first node. */
    const rrm_node_t * node = rrm_list_first(list);
    if (node == NULL) {
        /* There was not very much to sort. */
        return 0;
    }

    /* Link the copy of the first node as the first element of the
     * sorted copy. */
    rrm_receipt_t * receipt = rrm_node_receipt(node);
    rrm_node_t * first = rrm_node_new(receipt);
    if (first == NULL) {
        return 1;
    }
    rrm_list_link_first(sorted, first);

    rrm_node_t * prev = first;
    const rrm_receipt_t * prev_receipt = receipt;
    const rrm_receipt_t * first_receipt = receipt;
    for (node = rrm_node_next(node);
          node != NULL;
          node = rrm_node_next(node))
    {
        /* Create a copy of the node. */
        receipt = rrm_node_receipt(node);
        rrm_node_t * const copy = rrm_node_new(receipt);
        if (copy == NULL) {
            return 1;
        }

        if (receipt->amount < first_receipt->amount) {
            /* Link the copy as the first node, update the first
             * pointer and go on to the next node. */
            rrm_list_link_first(sorted, copy);
            first = copy;
            first_receipt = receipt;
            continue;
        }

        rrm_node_t * next = rrm_node_next(prev);
        const rrm_receipt_t * next_receipt;
        if (next == NULL) {
            if (receipt->amount >= prev_receipt->amount) {
                /* The copied node can be linked after the previous one
                 * directly. */
                rrm_node_link(prev, copy);
                continue;
            } else {
                goto restart;
            }
        }

        next_receipt = rrm_node_receipt(next);
        if (receipt->amount < prev_receipt->amount ||
             receipt->amount > next_receipt->amount)
        {
        restart:
            /* Step prev to the first node and update next before
             * going on to the while loop. */
            prev = first;
            prev_receipt = first_receipt;
            next = rrm_node_next(prev);
            next_receipt = rrm_node_receipt(next);
        }

        while (true) {
            if (receipt->amount >= prev_receipt->amount &&
                 receipt->amount <= next_receipt->amount)
            {
                rrm_node_link(prev, copy);
                break;
            }

            /* Step to the next receipt. */
            prev = next;
            prev_receipt = next_receipt;
            next = rrm_node_next(prev);
            if (next != NULL) {
                next_receipt = rrm_node_receipt(next);
            } else {
                /* The element has to be inserted last. */
                assert(receipt->amount >= prev_receipt->amount);
                rrm_node_link(prev, copy);
                break;
            }
        }
    }

    return 0;
}

int __attribute__((nonnull))
rrm_fit_repayment(const rrm_t * const rrm,
                  rrm_list_t * const list,
                  float * const best_amount,
                  const float amount)
{
    int return_value = 0;

    rrm_list_t free_nodes;
    rrm_list_t sorted;
    rrm_list_init(&free_nodes);
    rrm_list_init(&sorted);

    if (rrm_list_copy_sorted(&rrm->receipts, &sorted)) {
        goto error;
    }

    rrm_node_t * const best = rrm_fit_recursion(rrm_list_first(&sorted),
                                                &free_nodes,
                                                best_amount, amount);
    if (best == NULL) {
        goto error;
    }
    rrm_list_extend(list, best);

    if (false) {
    error:
        return_value = 1;
    }

    rrm_list_destroy(&free_nodes);
    rrm_list_destroy(&sorted);
    return return_value;
}
